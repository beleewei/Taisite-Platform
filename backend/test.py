from pymongo import MongoClient
from jinja2 import Markup, escape
# 连接MongoDB服务器
client = MongoClient('mongodb://47.98.192.123:27017/')

# 选择数据库
db = client['test']

# 创建集合
collection = db['test']

# 单个文档插入
collection.insert_one({"name": "John", "age": 30})

# 多个文档插入
data = [
    {"name": "Alice", "age": 25},
    {"name": "Bob", "age": 35}
]
collection.insert_many(data)

# 查询单个文档
result = collection.find_one({"name": "John"})
print('find_one',result)

# 查询多个文档
results = collection.find({"age": {"$gt": 30}})
for result in results:
    print('match:',result)


# 更新单个文档
# collection.update_one({"name": "John"}, {"$set": {"age": 40}})
# 更新多个文档
# collection.update_many({"age": {"$gt": 30}}, {"$inc": {"age": 1}})
# 删除单个文档
# collection.delete_one({"name": "John"})
# 删除多个文档
# collection.delete_many({"age": {"$gt": 30}})
